# ~/ABRÜPT/DONIA JORNOD/INTERFERENCES/*

La [page de ce livre](https://abrupt.ch/donia-jornod/interferences/) sur le réseau.

## Sur le livre

Du plastique brûlé, une valse interrompue, l'œil sur un royaume brisé, sur ses ruines virtuelles. Fantômes et interférences de leurs masques.

## Sur l'autrice

[Donia Jornod](https://doniajornod.org/) est apparue en 1991 avec les prémices du réseau. Artiste hologramme située à Zürich, ses œuvres côtoient les monstres en deçà des perceptions.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
